<?php
/**
* This plugin array is more or less self documenting
*/
$plugin = array(
  // the title in the admin
  'title' => t('Book TOC'),
  'required context' => new ctools_context_required(t('Node'), 'node'), 
  'content_types' => array('toclist'),
  'category' => t('Node'),
//  'edit form' => 'book_toc_content_type_edit_form',
//  'render callback' => 'book_toc_content_type_render',
);


/**
* Run-time rendering of the body of the block (content type)
* See ctools_plugin_examples for more advanced info
*/
function book_toc_toclist_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $block->title = t('Table of Contents');
  if(isset($context->data->book) && !empty($context->data->book['bid'])) {
    $pid = $context->data->book['bid'];
    $tree = menu_tree_all_data(book_menu_name($pid), NULL, $conf['book_toc_depth'] + 2);
    if (module_exists('i18n_book_navigation')) {
      $tree = i18n_book_navigation_translate_tree($tree);
    }
    $output = array_shift($tree);
    $content = menu_tree_output($output['below']);
    $block->content = drupal_render($content);       
  }
  return $block;
}


/**
* 'Edit form' callback for the content type.
*/
function book_toc_toclist_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['book_toc_depth'] = array(
    '#type' => 'select',
    '#title' => t('Table of Contents depth'),
    '#description' => t('The number of levels deep to display.'),
    '#default_value' => !empty($conf['book_toc_depth']) ? $conf['book_toc_depth'] : 2,
    '#options' => array(1,2,3,4,5),
  );
  // no submit
  return $form;
}


/**
* Submit function, note anything in the formstate[conf] automatically gets saved
*/
function book_toc_toclist_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['book_toc_depth'] = $form_state['values']['book_toc_depth'];
}

?>
