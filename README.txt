Book TOC
========

This small module provides a ctools plugin that displays the Table of Contents for a book. It can be added to and panel page with a node context and supports a configurable depth. It also provides translation support if i18n_book_navigation is installed.

INSTALLATION

1. enable module

2. typically you would use page manager node_view override to render the content-type for the book

3. in panels select Add content->Page content->Book TOC
